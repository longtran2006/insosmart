# Create your tasks here
from __future__ import absolute_import, unicode_literals

import os

import numpy
import pandas as pd
from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer

from insosmart_ml.carbotec.CarbotecAnalyzer import CarbotecKmeansAnalyzer

channel_layer = get_channel_layer()


@shared_task
def processing_task():
    number = 2
    print("Task done Long ABC!" + str(number))
    print("Task .... ! " + str(number))
    print("Try create task and predict")
    print(os.getcwd())
    trainning_data = pd.read_csv('data.csv')
    kmean_analyzer = CarbotecKmeansAnalyzer(6)

    cluster = kmean_analyzer.train_model(trainning_data)
    kmeans = pd.DataFrame(cluster)
    trainning_data.insert((trainning_data.shape[1]), 'kmeans', kmeans)
    trainning_data.to_csv('trained_data.csv')

    print(cluster[0:5])

    kmean_analyzer.save_model('kmean.pkl')

    another_test_data = numpy.array(
        [[64.62, 53.52, 42.9, 32.7, 22.2, 57.26, 0, 0, 0.62, 35.7, 45.52, 25.86, 16.76, 26, 99, 1]])

    new_kmean_analyzer = CarbotecKmeansAnalyzer.load_model('kmean.pkl')

    if new_kmean_analyzer is not None:
        cluster2 = new_kmean_analyzer.predict_model(another_test_data).tolist()
        print(cluster2)

        async_to_sync(channel_layer.group_send)(
            "statistics", {"type": "statistic.message", "message": cluster2}
        )

    return number * 3


"""
@shared_task
def import_data_task(filePath: str):
    print("Reading csv file")
    data = pd.read_csv(filePath)
    column_names = list(data.columns.values)
    save_data = 0
    process_datas = []
    measurements = []
    for index, row in data.iterrows():
        time_str = row['DateTime']
        time = datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S.%f')
        process_data = ProcessDataGroup(time_stamp=datetime.timestamp(time), system=None)
        process_datas.append(process_data)
        #process_data.save()
        for column in column_names:
            if column == 'DateTime':
                continue

            measurement = Measurement(name=column, value=float(row[column]), process_data_group=process_data)
            measurements.append(measurement)
            #measurement.save()

        save_data += 1
    ProcessDataGroup.objects.bulk_create(process_datas)
    Measurement.objects.bulk_create(measurements)

    print("saved " + str(save_data) + "process data rows")


@shared_task
def train_data_task():
    raw_data = {}
    for group in ProcessDataGroup.objects.all().iterator():
        for measurement in group.measurements.all():
            if measurement.name in raw_data.keys():
                raw_data[measurement.name].append(measurement.value)
            else:
                raw_data[measurement.name] = []
                raw_data[measurement.name].append(measurement.value)

    print("retrieved all data")

    df = pd.DataFrame.from_dict(raw_data)
    kmean_analyzer = CarbotecKmeansAnalyzer(6)
    cluster = kmean_analyzer.train_model(df.values)
    print("converted and trained data" + str(cluster.size))

    #using transaction for reducing multiple single update
    with transaction.atomic():
        for group in ProcessDataGroup.objects.all().iterator():
            group.label = cluster[group.id-1]
            group.save()

    print("Saved labels")
"""
