# Generated by Django 2.1.5 on 2019-04-21 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insosmart_statistics', '0011_carbotecmeasurement_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='carbotecmeasurement',
            name='first_comp',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='carbotecmeasurement',
            name='second_comp',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
