# Generated by Django 2.1.7 on 2019-03-23 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insosmart_statistics', '0003_adding_system'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processsystem',
            name='managers',
            field=models.ManyToManyField(blank=True, related_name='systems', to='insosmart_common.Manager'),
        ),
        migrations.AlterField(
            model_name='processsystem',
            name='operators',
            field=models.ManyToManyField(blank=True, related_name='systems', to='insosmart_common.Operator'),
        ),
    ]
