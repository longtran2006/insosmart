# Generated by Django 2.1.5 on 2019-04-20 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insosmart_statistics', '0008_measurement_label'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarbotecMeasurement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('TiFurnace', models.FloatField()),
                ('TiFlame', models.FloatField()),
                ('TDryerHotAir', models.FloatField()),
                ('TDryerInputAir', models.FloatField()),
                ('TDryer1', models.FloatField()),
                ('TDryer2', models.FloatField()),
                ('CO', models.FloatField()),
                ('PAugerFront', models.FloatField()),
                ('PAugerRear', models.FloatField()),
                ('TCarbon', models.FloatField()),
                ('TGas', models.FloatField()),
                ('CarbonAugerSpeed', models.FloatField()),
                ('FuelAugerSpeed', models.FloatField()),
                ('BurnerValve', models.FloatField()),
                ('FlareValve', models.FloatField()),
                ('DateTime', models.DateTimeField()),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
    ]
