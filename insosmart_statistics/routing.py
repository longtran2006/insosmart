from django.urls import path

from insosmart_statistics import consumers

websocket_urlpatterns = [
    path("ws/statistics/", consumers.StatisticConsumer)
]