from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.views.generic import TemplateView

from insosmart_statistics.plots import carbotec_statistic_plots
from insosmart_statistics.plots import plots


def statistic_list(request):
    context = {
        'nbar': "statistics_layout"
    }

    # import_data_from_csv.delay()

    return render(request, 'statistics.html', context)


class Plot1DView(TemplateView):
    template_name = "plot.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(Plot1DView, self).get_context_data(**kwargs)
        context['plot'] = plots.plot1d()
        return context


class Plot2DView(TemplateView):
    template_name = "plot.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(Plot2DView, self).get_context_data(**kwargs)
        context['plot'] = carbotec_statistic_plots.get_carbotec_heatmap_plot()
        return context


class Plot3DView(TemplateView):
    template_name = "plot.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(Plot3DView, self).get_context_data(**kwargs)
        # context['plot'] = plots.plot3d()
        context['plot'] = carbotec_statistic_plots.get_kmean_full_plots()
        return context


class Plot1DMultipleView(TemplateView):
    template_name = "plot_multiple.html"

    def get_context_data(self, **kwargs):
        n = int(kwargs['n'])
        # Call the base implementation first to get a context
        context = super(Plot1DMultipleView, self).get_context_data(**kwargs)
        context['plot'] = plots.plot1d_multiple(n)
        return context


def plot1d_multiple_ajax(request, n):
    """
    Only handles AJAX queries
    """
    return HttpResponse(plots.plot1d_multiple(int(n)))


class PlotIqView(TemplateView):
    template_name = "plot_fit.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PlotIqView, self).get_context_data(**kwargs)
        context['plot'] = plots.plotIq()
        return context


class PlotLiveView(TemplateView):
    template_name = "plot_live.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PlotLiveView, self).get_context_data(**kwargs)
        context['plot'] = plots.plotLive()
        return context


def plot_live_update(request):
    '''
    Handle ajax call to update the live plot
    '''
    if request.is_ajax():
        data = plots.live_plot_get_data_serialized()
        # In order to allow non-dict objects to be serialized set the safe
        # parameter to False
        return JsonResponse([data], safe=False)
    else:
        return HttpResponseBadRequest()


class Plot3DScatterView(TemplateView):
    template_name = "plot.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(Plot3DScatterView, self).get_context_data(**kwargs)
        context['plot'] = plots.plot3D_scatter
        return context


class PlotCarbotecClusterView(TemplateView):
    template_name = 'carbotec_cluster_plot.html'

    def get_context_data(self, **kwargs):
        context = super(PlotCarbotecClusterView, self).get_context_data(**kwargs)
        context['plot'] = carbotec_statistic_plots.get_carbotec_cluster_plot()
        return context


class PlotCarbotecHeatMapView(TemplateView):

    template_name = 'carbotec_cluster_plot.html'

    def get_context_data(self, **kwargs):
        context = super(PlotCarbotecHeatMapView, self).get_context_data(**kwargs)
        context['plot'] = carbotec_statistic_plots.get_carbotec_heatmap_plot()
        return context


def cluster_plot_update(request):
    '''
    Handle ajax call to update the live plot
    '''
    if request.is_ajax():
        first_comp_value = request.GET.get('first_comp', None)
        second_comp_value = request.GET.get('second_comp', None)
        data = carbotec_statistic_plots.live_plot_get_data_serialized(first_comp_value, second_comp_value)
        # In order to allow non-dict objects to be serialized set the safe
        # parameter to False
        return JsonResponse([data], safe=False)
    else:
        return HttpResponseBadRequest()

