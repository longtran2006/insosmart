import logging
import os

import numpy
import numpy as np
import pandas as pd
import plotly.graph_objs as go
from django.db.models.expressions import F
from plotly.offline import plot
import plotly.plotly as py
from insosmart_common.models import CarbotecMeasurement
from insosmart_ml.carbotec.CarbotecAnalyzer import CarbotecKmeansAnalyzer

logger = logging.getLogger(__name__)


def get_kmean_full_plots():
    trained_data = pd.read_csv('trained_data.csv')
    x = trained_data['TI-Furnace'].values
    y = trained_data['TI-Flame'].values
    z = trained_data['T-DryerHotAir'].values
    labels = trained_data['kmeans'].values

    trace = go.Scatter3d(
        x=x, y=y, z=z, mode='markers', name='points',
        marker=dict(
            color=labels.astype(np.float),
            line=dict(color='black', width=1))
    )

    data = [trace]
    layout = go.Layout(
        showlegend=False,
        autosize=False,
        width=800,
        height=700,
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        ),
        hovermode='closest',
        bargap=0,
    )

    fig = go.Figure(data=data, layout=layout)
    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
    return plot_div


def matplotlib_to_plotly(cmap, pl_entries):
    h = 1.0 / (pl_entries - 1)
    pl_colorscale = []

    for k in range(pl_entries):
        C = map(np.uint8, np.array(cmap(k * h)[:3]) * 255)
        pl_colorscale.append([k * h, 'rgb' + str((C[0], C[1], C[2]))])

    return pl_colorscale


def get_carbotec_cluster_plot():
    def matplotlib_to_plotly(cmap, pl_entries):
        h = 1.0 / (pl_entries - 1)
        pl_colorscale = []

        for k in range(pl_entries):
            C = map(np.uint8, np.array(cmap(k * h)[:3]) * 255)
            pl_colorscale.append([k * h, 'rgb' + str((C[0], C[1], C[2]))])

        return pl_colorscale

    data = CarbotecMeasurement.objects.filter(system__pk=2) \
        .annotate(idmod4=F('id') % 40) \
        .filter(idmod4=0) \
        .values('label', 'first_comp', 'second_comp')

    print(len(data))
    trained_data = pd.DataFrame.from_records(data)

    x = trained_data['first_comp'].values
    y = trained_data['second_comp'].values
    labels = trained_data['label'].values

    print(labels.min())
    print(labels.max())

    colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
                 3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'}
    cols = trained_data['label'].map(colorsIdx)
    """
    # Plot the decision boundary. For that, we will assign a color to each
    h = .02
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    new_kmean_analyzer = CarbotecKmeansAnalyzer.load_model('kmean.pkl')
    Z = new_kmean_analyzer.predict_model(np.c_[xx.ravel(), yy.ravel()]).tolist()
    print(Z)

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)

    back = go.Heatmap(x=xx[0][:len(Z)],
                      y=xx[0][:len(Z)],
                      z=Z,
                      showscale=False,
                      colorscale=matplotlib_to_plotly(plt.cm.Paired, len(Z)))
    """

    trace = go.Scatter(
        x=x, y=y, mode='markers', name='points',
        marker=dict(
            color=cols,
            line=dict(color='black', width=1))
    )

    data = [trace]
    layout = go.Layout(
        showlegend=False,
        autosize=False,
        width=800,
        height=700,
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        ),
        hovermode='closest',
        bargap=0,
    )

    fig = go.Figure(data=data)
    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
    return plot_div


def get_carbotec_heatmap_plot():
    model = CarbotecKmeansAnalyzer.load_model('kmean.pkl')
    if model is None or model.correlations is None:
        return ''

    print(model.correlations.values)
    trace = go.Heatmap(x=model.correlations.columns, y=model.correlations.columns, z=model.correlations.values)
    # trace = go.Heatmap(x=correlations.columns, y=correlations.columns, z=correlations)

    data = [trace]

    layout = dict(
        title = 'correlations between measurement',
        showlegend=True,
        autosize=True,
        margin=dict(
            l=125,
            r=25,
            b=125,
            t=50
        ),
        hovermode='closest',

    )

    fig = go.Figure(data=data, layout=layout)
    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
    return plot_div


def live_plot_get_data_serialized(first_comp, second_comp):

    model = CarbotecKmeansAnalyzer.load_model('kmean.pkl')
    predict_data = numpy.array(
        [[first_comp, second_comp]])

    result = list(model.predict_model(predict_data))
    colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
                 3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'}
    cols = map(colorsIdx, result)

    color = 'rgb(0,0,0)'
    if result[0] == 1:
        color = 'rgb(66, 134, 244)'
    elif result[0] == 2:
        color = 'rgb(151, 161, 178)'
    elif result[0] == 3:
        color = 'rgb(188, 59, 20)'
    elif result[0] == 4:
        color = 'rgb(181, 193, 13)'
    elif result[0] == 5:
        color = 'rgb(15, 193, 149)'

    data = dict(
        x= [first_comp], y=[second_comp], mode='markers', name='points',
        marker=dict(color=color, size=4, opacity=0.4)
    )
    return data


