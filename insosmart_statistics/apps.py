from django.apps import AppConfig


class InsosmartStatisticsConfig(AppConfig):
    name = 'insosmart_statistics'
