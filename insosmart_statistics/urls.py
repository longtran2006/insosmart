from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('', views.statistic_list, name='statistic_layout'),
    url(r'^plot1d/$', views.Plot1DView.as_view(), name='plot1d'),
    url(r'^plot2d/$', views.Plot2DView.as_view(), name='plot2d'),
    url(r'^plot3d/$', views.Plot3DView.as_view(), name='plot3d'),
    url(r'^plot1d_multiple/(?P<n>\d+)/$',
        views.Plot1DMultipleView.as_view(), name='plot1d_multiple'),
    url(r'^plot1d_multiple_ajax/(?P<n>\d+)/$',
        views.plot1d_multiple_ajax, name='plot1d_multiple_ajax'),
    url(r'^plotIq/$', views.PlotIqView.as_view(), name='plotIq'),
    url(r'^plot_live/$', views.PlotLiveView.as_view(), name='plot_live'),
    url(r'^plot_live_update/$', views.plot_live_update, name='plot_live_update'),
    url(r'^plot3d_scatter/$', views.Plot3DScatterView.as_view(), name='plot3d_scatter'),
    url(r'^plot_carbotec_cluster/$', views.PlotCarbotecClusterView.as_view(), name='plot_carbotec_cluster'),
    url(r'^plot_carbotec_heatmap/$', views.PlotCarbotecHeatMapView.as_view(), name='plot_carbotec_heatmap'),
    url(r'^carbotec_cluster_update/$', views.cluster_plot_update, name='carbotec_cluster_update'),

]
