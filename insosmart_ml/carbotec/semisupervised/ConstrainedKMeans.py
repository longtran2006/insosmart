import numpy as np



class KMeans:

    def __init__(self, n_clusters=6, max_iter=100):
        self.n_clusters = n_clusters
        self.max_iter = max_iter

    def fit(self, X, y=None, **kwargs):
        # Initialize cluster centers
        cluster_centers = self._init_cluster_centers(X, y)

        # Repeat until convergence
        for iteration in range(self.max_iter):
            prev_cluster_centers = cluster_centers.copy()

            # Assign clusters
            labels = self._assign_clusters(X, y, cluster_centers, self._dist)

            # Estimate means
            cluster_centers = self._get_cluster_centers(X, labels)

            # Check for convergence
            cluster_centers_shift = (prev_cluster_centers - cluster_centers)
            converged = np.allclose(cluster_centers_shift, np.zeros(cluster_centers.shape), atol=1e-6, rtol=0)

            if converged: break

        self.cluster_centers_, self.labels_ = cluster_centers, labels

        return self

    def predict(self, X):
        return self._assign_clusters(X, None, self.cluster_centers_, self._dist)

    def _init_cluster_centers(self, X, y=None):
        return X[np.random.choice(X.shape[0], self.n_clusters, replace=False), :]

    def _dist(self, x, y):
        return np.sqrt(np.sum((x - y) ** 2))

    def _assign_clusters(self, X, y, cluster_centers, dist):
        labels = np.full(X.shape[0], fill_value=-1)

        for i, x in enumerate(X):
            labels[i] = np.argmin([dist(x, c) for c in cluster_centers])

        n_samples_in_cluster = np.bincount(labels, minlength=self.n_clusters)
        empty_clusters = np.where(n_samples_in_cluster == 0)[0]

        if len(empty_clusters) > 0:
            raise Exception

        return labels

    def _get_cluster_centers(self, X, labels):
        cluster = []
        for i in range(self.n_clusters):
            assigned = X[labels == i]
            if len(assigned) == 0:
                ran = np.random.choice(X.shape[0], 1, replace=False)[0]
                cluster.append(X[ran, :])
            else:
                cluster.append(assigned.mean(axis=0))

        return np.array(cluster)
        #return np.array([X[labels == i].mean(axis=0) for i in range(self.n_clusters)])


class SeededKMeans(KMeans):
    def _init_cluster_centers(self, X, y=None):
        if np.all(y is None or y == -1):
            return X[np.random.choice(X.shape[0], self.n_clusters, replace=False), :]
        else:
            return self._get_cluster_centers(X, y)


class ConstrainedKMeans(SeededKMeans):
    def _assign_clusters(self, X, y, cluster_centers, dist):
        labels = np.full(X.shape[0], fill_value=-1)

        for i, x in enumerate(X):
            if y[i] != -1:
                labels[i] = y[i]
            else:
                labels[i] = np.argmin([dist(x, c) for c in cluster_centers])

        n_samples_in_cluster = np.bincount(labels, minlength=self.n_clusters)
        empty_clusters = np.where(n_samples_in_cluster == 0)[0]

        if len(empty_clusters) > 0:
            raise Exception

        return labels
