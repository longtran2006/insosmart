import numpy

from insosmart_ml.carbotec.experiments.KMean import TFKMeansCluster

if __name__ == '__main__':
    data = numpy.loadtxt(open("data.csv", "rb"), delimiter=",", skiprows=1)

    trainned, assignments = TFKMeansCluster(data,6)
    print("done")

