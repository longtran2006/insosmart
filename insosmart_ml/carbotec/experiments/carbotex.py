import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans

from sklearn.decomposition import PCA
from sklearn.manifold import Isomap
from sklearn.manifold import TSNE

# K means Clustering
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import StandardScaler


def doKmeans(X, nclust=2):
    model = KMeans(nclust)
    model.fit(X)
    clust_labels = model.predict(X)
    cent = model.cluster_centers_
    return (clust_labels, cent)


if __name__ == '__main__':
    carbofex = pd.read_csv('data.csv')
    carbofex.info()
    carbofex1 = carbofex[
        ['TI-Furnace',
         'TI-Flame',
         'T-DryerHotAir',
         'T-DryerInputAir',
         'T-Dryer1',
         'T-Dryer2',
         'CO',
         'P-AugerFront',
         'P-AugerRear',
         'T-carbon',
         'T-gas',
         'CarbonAugerSpeed',
         'FuelAugerSpeed',
         'BurnerValve',
         'FlareValve',
         'WorkingShift'
         ]
    ]
    for i in carbofex1.columns:
        plt.figure(figsize=(8, 8))
        carbofex1[i].hist()
        plt.xlabel(str(i))
        plt.ylabel("Frequency")

    corrmat = carbofex1.corr(method='spearman')
    f, ax = plt.subplots(figsize=(8, 8))

    # Draw the heatmap using seaborn
    sns.heatmap(corrmat, vmax=.8, square=True)
    plt.show()

    # using something:

    # Scaling of data
    ss = StandardScaler()
    ss.fit_transform(carbofex1)

    carbofex2 = carbofex1

    """
    for n in [4, 5, 6, 7, 8]:
        clust_labels, cent = doKmeans(carbofex1, n)
        kmeans = pd.DataFrame(clust_labels)
        #carbofex1.insert((carbofex1.shape[1]), 'kmeans', kmeans)

        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg = silhouette_score(carbofex1, clust_labels)

        print("For NORMAL n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg)

    """
    RS = 20190307
    # X_tsne = TSNE(random_state=RS, learning_rate=5, metric="euclidean").fit_transform(carbofex)
    X_pca = PCA(n_components="mle", svd_solver='full').fit_transform(carbofex2)
    for n in [2, 3, 4, 5, 6, 7, 8]:
        clust_labels2, cent2 = doKmeans(X_pca, n)
        kmeans2 = pd.DataFrame(clust_labels2)
        #X_pca.insert((X_pca.shape[1]), 'kmeans', kmeans2)

        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg2 = silhouette_score(X_pca, clust_labels2)

        print("For PCA n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg2)