import numpy
import matplotlib.pyplot as plt  # For graphics
import pandas as pd  # Dataframe manipulation
import seaborn as sns
from sklearn.cluster import KMeans, AffinityPropagation  # For clustering
from sklearn.preprocessing import StandardScaler  # For scaling dataset
from joblib import dump, load


# K means Clustering
def train_and_save_with_kmeans(X, nclust=2):
    model = KMeans(nclust)
    model.fit(X)
    dump(model, 'kmean.joblib')

    cluster_labels = model.predict(X)
    center = model.cluster_centers_
    return cluster_labels, center


def load_and_predict_with_kmeans(x):
    model = load('kmean.joblib')
    labels = model.predict(x)
    return labels

"""

It does not require the number of cluster to be estimated and provided before starting the algorithm.
 It makes no assumption regarding the internal structure of the data points. 
 For further details on clustering, refer 
 http://www.learndatasci.com/k-means-clustering-algorithms-python-intro/
"""


def train_with_affinity(X):
    model = AffinityPropagation(damping=0.5, max_iter=1, affinity='euclidean')
    model.fit(X)
    cluster_label = model.predict(X)
    center = model.cluster_centers_
    return cluster_label, center


if __name__ == '__main__':
    predicted_data = numpy.array([[0, 0]])

    for i in range(0, 100):
        for j in range(0, 100):
            if i == 0 and j == 0:
                continue
            predicted_data = numpy.append(predicted_data, [[i, j]], axis=0)
    x1 = predicted_data[:, 0]
    y1 = predicted_data[:, 1]

    carbofex = pd.read_csv('data.csv')
    stat = carbofex.describe()
    test_data = carbofex.iloc[:10]
    print(stat)

    print(carbofex.dtypes)
    carbofex1 = carbofex[
        ['TI-Furnace',
         'TI-Flame',
         'T-DryerHotAir',
         'T-DryerInputAir',
         'T-Dryer1',
         'T-Dryer2',
         'CO',
         'P-AugerFront',
         'P-AugerRear',
         'T-carbon',
         'T-gas',
         'CarbonAugerSpeed',
         'FuelAugerSpeed',
         'BurnerValve',
         'FlareValve',
         'WorkingShift'
         ]
    ]

    cor = carbofex1.corr()  # Calculate the correlation of the above variables
    heatMap = sns.heatmap(cor, square=True)  # Plot the correlation as heat map

    figureHeatMap = heatMap.get_figure()
    figureHeatMap.savefig('heatmap.png', dpi=1000)

    # plt.plot(heatMap)
    # Scaling of data
    ss = StandardScaler()
    ss.fit_transform(carbofex1)


    clust_labels, cent = train_and_save_with_kmeans(carbofex1, 6)
    print(clust_labels[0:10])
    kmeans = pd.DataFrame(clust_labels)
    carbofex1.insert((carbofex1.shape[1]), 'kmeans', kmeans)

    # Plot the clusters obtained using k means
    fig = plt.figure()
    ax = fig.add_subplot(111)
    scatter = ax.scatter(carbofex1['TI-Furnace'], carbofex1['TI-Flame'], c=kmeans[0], s=50)

    ax.set_title('K-Means Clustering')
    ax.set_xlabel('Furnace temperature')
    ax.set_ylabel('Flame')
    plt.colorbar(scatter)

    plt.savefig('result.png')

    ss.fit_transform(test_data)
    clust_labels1 = load_and_predict_with_kmeans(test_data)

    print(clust_labels1)
    another_test_data = numpy.array([[64.62,53.52,42.9,32.7,22.2,57.26,0,0,0.62,35.7,45.52,25.86,16.76,26,99,1]])

    clust_labels2 = load_and_predict_with_kmeans(another_test_data)
    print(clust_labels2)
