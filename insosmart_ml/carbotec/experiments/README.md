A machine learning service for Carbotec project. 

* For NORMAL n_clusters = 4 The average silhouette_score is : 0.6996382240972527
* For NORMAL n_clusters = 5 The average silhouette_score is : 0.7613570157040745
* For NORMAL n_clusters = 6 The average silhouette_score is : 0.7646576428413661
* For NORMAL n_clusters = 7 The average silhouette_score is : 0.7080921952971584
* For NORMAL n_clusters = 8 The average silhouette_score is : 0.708237510065255

* For PCA n_clusters = 2 The average silhouette_score is : 0.5674249836707563
* For PCA n_clusters = 3 The average silhouette_score is : 0.6151387904096598
* For PCA n_clusters = 4 The average silhouette_score is : 0.6996460107254202
* For PCA n_clusters = 5 The average silhouette_score is : 0.7613653618968457
* For PCA n_clusters = 6 The average silhouette_score is : 0.7646660028343498
* For PCA n_clusters = 7 The average silhouette_score is : 0.7081500847295347
* For PCA n_clusters = 8 The average silhouette_score is : 0.7082963687851472