import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.engine.training import Model
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.optimizers import Adam


def doKmeans(X, nclust=2):
    model = KMeans(nclust)
    model.fit(X)
    clust_labels = model.predict(X)
    cent = model.cluster_centers_
    return (clust_labels, cent)


if __name__ == '__main__':
    carbofex = pd.read_csv('data.csv')
    carbofex.info()
    carbofex1 = carbofex[
        ['TI-Furnace',
         'TI-Flame',
         'T-DryerHotAir',
         'T-DryerInputAir',
         'T-Dryer1',
         'T-Dryer2',
         'CO',
         'P-AugerFront',
         'P-AugerRear',
         'T-carbon',
         'T-gas',
         'CarbonAugerSpeed',
         'FuelAugerSpeed',
         'BurnerValve',
         'FlareValve',
         'WorkingShift'
         ]
    ]
    # Scaling of data
    ss = StandardScaler()
    ss.fit_transform(carbofex1)

    x_train, x_test = train_test_split(carbofex1, test_size=0.2)
    m = Sequential()
    m.add(Dense(12, activation='linear', input_shape=(16,)))
    m.add(Dense(8, activation='linear'))
    m.add(Dense(4, activation='linear'))
    m.add(Dense(2, activation='linear', name="bottleneck"))
    m.add(Dense(4, activation='linear'))
    m.add(Dense(8, activation='linear'))
    m.add(Dense(12, activation='linear'))
    m.add(Dense(16, activation='linear'))
    m.compile(loss='mean_squared_error', optimizer=Adam())
    history = m.fit(x_train, x_train, batch_size=64, epochs=10, verbose=1, validation_data=(x_test, x_test))
    encoder = Model(m.input, m.get_layer('bottleneck').output)
    Zenc = encoder.predict(x_train)  # bottleneck representation
    Renc = m.predict(x_train)  # reconstruction

    clust_labels2, cent2 = doKmeans(Zenc, 6)
    kmeans2 = pd.DataFrame(clust_labels2)
    # X_pca.insert((X_pca.shape[1]), 'kmeans', kmeans2)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    # silhouette_avg2 = silhouette_score(Zenc, clust_labels2)

    # print("For PCA n_clusters =", 6,
    #      "The average silhouette_score is :", silhouette_avg2)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    scatter = ax.scatter(Zenc[:, 0], Zenc[:, 1], c=kmeans2[0], s=50)

    ax.set_title('K-Means Clustering')
    ax.set_xlabel('Furnace temperature')
    ax.set_ylabel('Flame')
    plt.colorbar(scatter)

    plt.savefig('result1.png')
