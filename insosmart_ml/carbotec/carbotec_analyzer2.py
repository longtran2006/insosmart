import random

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from ivis import Ivis
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans, AffinityPropagation  # For clustering
from sklearn.preprocessing import StandardScaler, MinMaxScaler  # For scaling dataset
from sklearn.metrics import silhouette_score


# K means Clustering
def train_and_save_with_kmeans(X, nclust=2):
    model = KMeans(nclust)
    model.fit(X)

    cluster_labels = model.predict(X)
    center = model.cluster_centers_
    return cluster_labels, center


if __name__ == "__main__":
    X = pd.read_csv("data.csv")
    X = X.drop(columns=['T-DryerHotAir', 'T-DryerInputAir', 'CarbonAugerSpeed', 'FuelAugerSpeed'])

    X_scaled = MinMaxScaler().fit_transform(X)
    # Set ivis parameters
    model = Ivis(k=100, n_epochs_without_progress=10, model='maaten')
    # result 4: k = 20 ivis1
    # result 5 ; k = 100 ivis2
    # result 6: k = 150 ivis3
    # result 7: k = 50 ivis4
    # Generate embeddings
    # embeddings = model.fit_transform(X_scaled)

    model.load_model('iris2.ivis2')
    embeddings = model.transform(X_scaled)

    for n in [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]:
        clust_labels2, cent2 = train_and_save_with_kmeans(embeddings, n)
        kmeans2 = pd.DataFrame(clust_labels2)

        silhouette_avg2 = silhouette_score(embeddings, clust_labels2)

        print("For PCA n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg2)


    model = Ivis(k=150, n_epochs_without_progress=10, model='maaten')
    model.load_model('iris3.ivis3')
    embeddings = model.transform(X_scaled)

    for n in [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]:
        clust_labels2, cent2 = train_and_save_with_kmeans(embeddings, n)
        kmeans2 = pd.DataFrame(clust_labels2)

        silhouette_avg2 = silhouette_score(embeddings, clust_labels2)

        print("For PCA n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg2)



    model = Ivis(k=50, n_epochs_without_progress=10, model='maaten')
    model.load_model('iris4.ivis4')
    embeddings = model.transform(X_scaled)

    for n in [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]:
        clust_labels2, cent2 = train_and_save_with_kmeans(embeddings, n)
        kmeans2 = pd.DataFrame(clust_labels2)

        silhouette_avg2 = silhouette_score(embeddings, clust_labels2)

        print("For PCA n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg2)


    clust_labels2, cent2 = train_and_save_with_kmeans(embeddings, 6)
    kmeans2 = pd.DataFrame(clust_labels2)
    colors = np.ones(len(embeddings[:, 0]))

    plt.figure(figsize=(6, 4), dpi=150)
    sc = plt.scatter(x=embeddings[:, 0], y=embeddings[:, 1], c=kmeans2[0], s=5)
    plt.xlabel('ivis 1')
    plt.ylabel('ivis 2')
    plt.title('CST3')
    plt.colorbar(sc)

    plt.savefig('result41.png')

    # Export model
    # model.save_model('iris4.ivis4')

    """
    iris1 
    
    For PCA n_clusters = 2 The average silhouette_score is : 0.35301903
For PCA n_clusters = 3 The average silhouette_score is : 0.3465349
For PCA n_clusters = 4 The average silhouette_score is : 0.33895388
For PCA n_clusters = 5 The average silhouette_score is : 0.3476308
For PCA n_clusters = 6 The average silhouette_score is : 0.34048867
For PCA n_clusters = 7 The average silhouette_score is : 0.35465935
For PCA n_clusters = 8 The average silhouette_score is : 0.3663341
For PCA n_clusters = 9 The average silhouette_score is : 0.36602995
For PCA n_clusters = 10 The average silhouette_score is : 0.3712556
For PCA n_clusters = 11 The average silhouette_score is : 0.36974627

    iris2
    For PCA n_clusters = 2 The average silhouette_score is : 0.40847224
For PCA n_clusters = 3 The average silhouette_score is : 0.39619365
For PCA n_clusters = 4 The average silhouette_score is : 0.4066409
For PCA n_clusters = 5 The average silhouette_score is : 0.43677953
For PCA n_clusters = 6 The average silhouette_score is : 0.4255328
For PCA n_clusters = 7 The average silhouette_score is : 0.41015297
For PCA n_clusters = 8 The average silhouette_score is : 0.4180166
For PCA n_clusters = 9 The average silhouette_score is : 0.42399344
For PCA n_clusters = 10 The average silhouette_score is : 0.4121198
For PCA n_clusters = 11 The average silhouette_score is : 0.4088413
    iris3
For PCA n_clusters = 2 The average silhouette_score is : 0.43101013
For PCA n_clusters = 3 The average silhouette_score is : 0.4361326
For PCA n_clusters = 4 The average silhouette_score is : 0.43573245
For PCA n_clusters = 5 The average silhouette_score is : 0.41576022
For PCA n_clusters = 6 The average silhouette_score is : 0.41145203
For PCA n_clusters = 7 The average silhouette_score is : 0.42147285
For PCA n_clusters = 8 The average silhouette_score is : 0.4233486
For PCA n_clusters = 9 The average silhouette_score is : 0.43807477
For PCA n_clusters = 10 The average silhouette_score is : 0.45595458
For PCA n_clusters = 11 The average silhouette_score is : 0.44508347

    iris4:
    For PCA n_clusters = 2 The average silhouette_score is : 0.37248042
For PCA n_clusters = 3 The average silhouette_score is : 0.40731907
For PCA n_clusters = 4 The average silhouette_score is : 0.4409455
For PCA n_clusters = 5 The average silhouette_score is : 0.42780635
For PCA n_clusters = 6 The average silhouette_score is : 0.41297746
For PCA n_clusters = 7 The average silhouette_score is : 0.41889006
For PCA n_clusters = 8 The average silhouette_score is : 0.40433887
For PCA n_clusters = 9 The average silhouette_score is : 0.39661518
For PCA n_clusters = 10 The average silhouette_score is : 0.396197
For PCA n_clusters = 11 The average silhouette_score is : 0.39204928
    """