import random

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans, AffinityPropagation  # For clustering
from sklearn.preprocessing import StandardScaler  # For scaling dataset
from sklearn.metrics import silhouette_score

# K means Clustering
def train_and_save_with_kmeans(X, nclust=2):
    model = KMeans(nclust)
    model.fit(X)

    cluster_labels = model.predict(X)
    center = model.cluster_centers_
    return cluster_labels, center

if __name__ == "__main__":
    train = pd.read_csv("data.csv")
    # checking the percentage of missing values in each variable
    missingValues = train.isnull().sum() / len(train) * 100
    print("Missing values")
    print(missingValues)

    #Low variance filter
    variance = train.var()
    print("Low variance")
    print(variance)

    numeric = train.columns
    variable = []
    for i in range(0, len(variance)):
        a = variance[i]
        if (a - 10) >= 0:  # setting the threshold as 10%
            variable.append(numeric[i])

    print(variable)
    train = train[variable]
    print(train.head())

    #High correlation filter
    correlation = train.corr()
    print(correlation)
    train = train.drop(columns=['T-DryerHotAir', 'T-DryerInputAir', 'CarbonAugerSpeed', 'FuelAugerSpeed'])

    print(train.head())
    """
    #Kmeans training
    train1 = train;
    ss = StandardScaler()
    ss.fit_transform(train1)
    for n in [4, 5, 6, 7, 8]:
        clust_labels, cent = train_and_save_with_kmeans(train1, n)
        kmeans = pd.DataFrame(clust_labels)

        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg = silhouette_score(train1, clust_labels)

        print("For NORMAL n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg)



    pca = PCA(n_components=2)
    pca_result = pca.fit_transform(train.values)
    for n in [4, 5, 6, 7, 8]:
        clust_labels2, cent2 = train_and_save_with_kmeans(pca_result, n)
        kmeans2 = pd.DataFrame(clust_labels2)

        silhouette_avg2 = silhouette_score(pca_result, clust_labels2)

        print("For PCA n_clusters =", n,
              "The average silhouette_score is :", silhouette_avg2)
    """
    pca = PCA(n_components=2)
    pca_result = pca.fit_transform(train.values)

    clust_labels2, cent2 = train_and_save_with_kmeans(pca_result, 6)
    kmeans2 = pd.DataFrame(clust_labels2)
    train.insert((train.shape[1]), 'kmeans', kmeans2)
    #add new column "correct_label"
    train['correct_label'] = -1
    #trainUpdate = train.sample(1000)
    #trainUpdate['correct_label'] = random.randint(0, 5)
    #train.update(trainUpdate)
    #update_list = trainUpdate.index.tolist()

    trainUpdate = train.sample(2500).index
    value1 = random.randint(0, 2)
    train.loc[trainUpdate, 'correct_label'] = value1

    trainUpdate2 = train.sample(2500).index
    value2 = random.randint(2, 4)
    train.loc[trainUpdate2, 'correct_label'] = value2

    trainUpdate3 = train.sample(1000).index
    value3 = random.randint(4, 5)
    train.loc[trainUpdate3, 'correct_label'] = value3
    train.to_csv('semi_label.csv')


    fig = plt.figure()
    ax = fig.add_subplot(111)
    scatter = ax.scatter(pca_result[:, 0], pca_result[:, 1], c=train['correct_label'], s=50)

    ax.set_title('K-Means Clustering')
    plt.colorbar(scatter)

    plt.savefig('result2.png')