from abc import ABC, abstractmethod
import tensorflow as tf
import numpy as np
import pandas as pd
from ivis import Ivis
from joblib import dump, load
from numpy.core.multiarray import ndarray
from sklearn.cluster import KMeans
from sklearn.utils import shuffle
from sklearn.preprocessing import StandardScaler, MinMaxScaler

from insosmart_ml.carbotec.semisupervised.ConstrainedKMeans import ConstrainedKMeans


class CarbotecAnalyzer(ABC):

    def __init__(self):
        self.trained = False

    @abstractmethod
    def train_model(self, data):
        pass

    @abstractmethod
    def semi_train_model(self, data, labels):
        pass

    @abstractmethod
    def save_model(self, file_path: str):
        pass

    @abstractmethod
    def predict_model(self, predicted_data: ndarray):
        pass

    @staticmethod
    @abstractmethod
    def load_model(file_path: str) -> 'CarbotecAnalyzer':
        pass


class CarbotecKmeansAnalyzer(CarbotecAnalyzer):

    def semi_train_model(self, data, labels):
        pass

    def __init__(self, number_of_cluster: int):
        super().__init__()
        self.number_of_cluster = number_of_cluster
        self.model = KMeans(number_of_cluster)
        self.correlations = []

    def train_model(self, data: ndarray, transform: bool = True) -> object:
        # Scaling of data
        if transform:
            ss = StandardScaler()
            ss.fit_transform(data)

        self.model.fit(data)

        cluster_labels = self.model.predict(data)
        self.trained = True
        return cluster_labels

    def save_model(self, file_path: str):
        dump(self, file_path)

    def predict_model(self, predicted_data: ndarray):
        if not self.trained:
            raise Exception("the analyzer haven't trained yet, please train first")

        cluster_labels = self.model.predict(predicted_data)
        return cluster_labels

    @staticmethod
    def load_model(file_path: str) -> 'CarbotecKmeansAnalyzer':
        try:
            kmean_analyzer = load(file_path)
            return kmean_analyzer
        except:
            return None


class CarbotecConstrainedKmeansAnalyzer(CarbotecAnalyzer):

    def __init__(self, number_of_cluster: int):
        super().__init__()
        self.number_of_cluster = number_of_cluster
        self.model = ConstrainedKMeans(number_of_cluster)

    def train_model(self, data: ndarray, transform: bool = True) -> object:
        # Scaling of data
        if transform:
            ss = StandardScaler()
            ss.fit_transform(data)

        self.model.fit(data)

        cluster_labels = self.model.labels_
        self.trained = True
        return cluster_labels

    def semi_train_model(self, data, labels, transform: bool = True) -> object:
        # Scaling of data
        if transform:
            ss = StandardScaler()
            ss.fit_transform(data)

        self.model.fit(data, labels)

        cluster_labels = self.model.labels_
        self.trained = True
        return cluster_labels

    def save_model(self, file_path: str):
        dump(self, file_path)

    def predict_model(self, predicted_data: ndarray):
        if not self.trained:
            raise Exception("the analyzer haven't trained yet, please train first")

        cluster_labels = self.model.predict(predicted_data)
        return cluster_labels

    @staticmethod
    def load_model(file_path: str) -> 'CarbotecKmeansAnalyzer':
        try:
            kmean_analyzer = load(file_path)
            return kmean_analyzer
        except:
            return None


class CarbotecIvisAnalyzer(CarbotecAnalyzer):

    def __init__(self, number_of_cluster: int):
        super().__init__()
        self.number_of_cluster = number_of_cluster

        self.ivisModel = Ivis(k=50, n_epochs_without_progress=10, epochs=20, model='maaten')
        self.model = KMeans(number_of_cluster)
        self.correlations = []

    def train_model(self, data: ndarray, transform: bool = True) -> object:
        # Scaling of data
        if transform:
            data = MinMaxScaler().fit_transform(data)
            try:
                self.ivisModel = self.ivisModel.load_model('iris5.ivis5')
                embeddings = self.ivisModel.transform(data)
            except:
                embeddings = self.ivisModel.fit_transform(data)
                self.ivisModel.save_model('iris5.ivis5')

        self.model.fit(embeddings)

        cluster_labels = self.model.predict(embeddings)
        self.trained = True
        return embeddings, cluster_labels

    def semi_train_model(self, data, labels):
        pass

    def save_model(self, file_path: str):
        dump(self, file_path)

    def predict_model(self, predicted_data: ndarray):
        if not self.trained:
            raise Exception("the analyzer haven't trained yet, please train first")

        try:
            self.ivisModel = self.ivisModel.load_model('iris5.ivis5')
            predicted_data = self.ivisModel.transform(predicted_data)
        except:
            raise Exception("the analyzer haven't trained yet, please train first")

        cluster_labels = self.model.predict(predicted_data)
        return cluster_labels

    @staticmethod
    def load_model(file_path: str) -> 'CarbotecIvisAnalyzer':
        try:
            ivis_analyzer = load(file_path)
            return ivis_analyzer
        except:
            return None


class FNN():

    def __init__(self, input_dim, hidden_dim, act, d_act):
        self.w = tf.Variable(tf.random_normal([input_dim, hidden_dim], stddev=0.05, seed=2))
        self.m, self.v_prev = tf.Variable(tf.zeros_like(self.w)), tf.Variable(tf.zeros_like(self.w))
        self.v_hat_prev = tf.Variable(tf.zeros_like(self.w))
        self.act, self.d_act = act, d_act

    def feedforward(self, input=None):
        self.input = input
        self.layer = tf.matmul(input, self.w)
        self.layerA = self.act(self.layer)
        return self.layerA

    def backprop(self, gradient=None, learning_rate=0.0000001, beta1=0.9, beta2=0.999, adam_e=1e-8):
        grad_part_1 = gradient
        grad_part_2 = self.d_act(self.layer)
        grad_part_3 = self.input

        grad_middle = grad_part_1 * grad_part_2
        grad = tf.matmul(tf.transpose(grad_part_3), grad_middle)
        grad_pass = tf.matmul(tf.multiply(grad_part_1, grad_part_2), tf.transpose(self.w))

        update_w = []
        update_w.append(tf.assign(self.m, self.m * beta1 + (1 - beta1) * (grad)))
        update_w.append(tf.assign(self.v_prev, self.v_prev * beta2 + (1 - beta2) * (grad ** 2)))
        m_hat = self.m / (1 - beta1)
        v_hat = self.v_prev / (1 - beta2)
        adam_middel = learning_rate / (tf.sqrt(v_hat) + adam_e)
        update_w.append(tf.assign(self.w, tf.subtract(self.w, tf.multiply(adam_middel, m_hat))))

        return grad_pass, update_w


class SOMLayer:

    def tf_softmax(self, x):
        return tf.nn.softmax(x)

    def __init__(self, m, n, dim, num_epoch, learning_rate_som=0.04, radius_factor=1.1, gaussian_std=0.5):

        self.m = m
        self.n = n
        self.dim = dim
        self.gaussian_std = gaussian_std
        self.num_epoch = num_epoch
        self.map = tf.Variable(tf.random_uniform(shape=[m * n, dim], minval=0.0, maxval=1.0, seed=2))

        self.location_vects = tf.constant(np.array(list(self._neuron_locations(m, n))))
        self.alpha = learning_rate_som
        self.sigma = max(m, n) * 1.1

    def _neuron_locations(self, m, n):
        """
        Yields one by one the 2-D locations of the individual neurons in the SOM.
        """
        # Nested iterations over both dimensions to generate all 2-D locations in the map
        for i in range(m):
            for j in range(n):
                yield np.array([i, j])

    def getmap(self):
        return self.map

    def getlocation(self):
        return self.bmu_locs

    def feedforward(self, input):
        self.input = input
        self.grad_pass = tf.pow(tf.subtract(tf.expand_dims(self.map, axis=0), tf.expand_dims(self.input, axis=1)), 2)
        self.squared_distance = tf.reduce_sum(self.grad_pass, 2)
        self.bmu_indices = tf.argmin(self.squared_distance, axis=1)
        self.bmu_locs = tf.reshape(tf.gather(self.location_vects, self.bmu_indices), [-1, 2])

    def backprop(self, iter, num_epoch):

        # Update the weigths
        radius = tf.subtract(self.sigma,
                             tf.multiply(iter,
                                         tf.divide(tf.cast(tf.subtract(self.alpha, 1), tf.float32),
                                                   tf.cast(tf.subtract(num_epoch, 1), tf.float32))))

        alpha = tf.subtract(self.alpha,
                            tf.multiply(iter,
                                        tf.divide(tf.cast(tf.subtract(self.alpha, 1), tf.float32),
                                                  tf.cast(tf.subtract(num_epoch, 1), tf.float32))))

        self.bmu_distance_squares = tf.reduce_sum(
            tf.pow(tf.subtract(
                tf.expand_dims(self.location_vects, axis=0),
                tf.expand_dims(self.bmu_locs, axis=1)), 2),
            2)

        self.neighbourhood_func = tf.exp(tf.divide(tf.negative(tf.cast(
            self.bmu_distance_squares, "float32")), tf.multiply(
            tf.square(tf.multiply(radius, self.gaussian_std)), 2)))

        self.learning_rate_op = tf.multiply(self.neighbourhood_func, alpha)

        self.numerator = tf.reduce_sum(
            tf.multiply(tf.expand_dims(self.learning_rate_op, axis=-1),
                        tf.expand_dims(self.input, axis=1)), axis=0)

        self.denominator = tf.expand_dims(
            tf.reduce_sum(self.learning_rate_op, axis=0) + float(1e-20), axis=-1)

        self.new_weights = tf.div(self.numerator, self.denominator)
        self.update = [tf.assign(self.map, self.new_weights)]

        return self.update, tf.reduce_mean(self.grad_pass, 1)


class CarbotecSomAnalyzer(CarbotecAnalyzer):

    def tf_elu(self, x):
        return tf.nn.elu(x)

    def d_tf_elu(self, x):
        return tf.cast(tf.greater(x, 0), tf.float32) + (
                self.tf_elu(tf.cast(tf.less_equal(x, 0), tf.float32) * x) + 1.0)

    def tf_tanh(self, x):
        return tf.nn.tanh(x)

    def d_tf_tanh(self, x):
        return 1 - self.tf_tanh(x) ** 2

    def tf_sigmoid(self, x):
        return tf.nn.sigmoid(x)

    def d_tf_sigmoid(self, x):
        return self.tf_sigmoid(x) * (1.0 - self.tf_sigmoid(x))

    def tf_atan(self, x):
        return tf.atan(x)

    def d_tf_atan(self, x):
        return 1.0 / (1.0 + x ** 2)

    def tf_iden(self, x):
        return x

    def d_tf_iden(self, x):
        return 1.0

    def __init__(self, number_of_cluster: int):
        super().__init__()
        self.map_dim = 2
        self.map_width_height = 100
        self.batch_size = 1000
        self.num_epoch = 20
        self.model = KMeans(number_of_cluster)

    def train_model(self, data: ndarray, transform: bool = True) -> object:
        # Scaling of data
        batch_size = self.batch_size

        el1 = FNN(10, 8, act=self.tf_atan, d_act=self.d_tf_atan)
        el2 = FNN(8, 6, act=self.tf_atan, d_act=self.d_tf_atan)
        el3 = FNN(6, 4, act=self.tf_atan, d_act=self.d_tf_atan)
        el4 = FNN(4, 2, act=self.tf_atan, d_act=self.d_tf_atan)
        som_layer = SOMLayer(self.map_width_height, self.map_width_height, self.map_dim,
                             num_epoch=self.num_epoch, learning_rate_som=0.8,
                             radius_factor=1.1, gaussian_std=0.08)

        # graph
        x = tf.placeholder(shape=[None, 10], dtype=tf.float32, name="input")
        current_iter = tf.placeholder(shape=[], dtype=tf.float32)

        saver = tf.train.Saver()

        # encoder
        elayer1 = el1.feedforward(x)
        elayer2 = el2.feedforward(elayer1)
        elayer3 = el3.feedforward(elayer2)
        elayer4 = el4.feedforward(elayer3)
        som_layer.feedforward(elayer4)

        som_update, som_grad = som_layer.backprop(current_iter, self.num_epoch)
        egrad4, egrad4_up = el4.backprop(som_grad)
        egrad3, egrad3_up = el3.backprop(egrad4)
        egrad2, egrad2_up = el2.backprop(egrad3)
        egrad1, egrad1_up = el1.backprop(egrad2)
        grad_update = som_update + egrad4_up + egrad3_up + egrad2_up + egrad1_up

        with tf.Session() as sess:
            if transform:
                sess.run(tf.global_variables_initializer())
                # start the training
                for iteration in range(self.num_epoch):
                    train_batch = shuffle(data)
                    for batch_size_index in range(0, len(train_batch), batch_size):
                        current_batch = train_batch[batch_size_index:batch_size_index + batch_size]
                        sess_results = sess.run(grad_update, feed_dict={x: current_batch, current_iter: iteration})
                        #print('Current Iter: ', iteration, ' Current Train Index: ', batch_size_index,
                        #      ' Current SUM of updated Values: ', np.mean(sum(sess_results[0])), end='\r')
                    # print('\n-----------------------')
            else:
                self.saver.restore(sess, 'model/long_som_model.ckpt')

            # get the trained map and normalize
            trained_map = sess.run(som_layer.getmap())
            self.model.fit(trained_map)
            cluster_labels = self.model.predict(trained_map).reshape(self.map_width_height, self.map_width_height)

            locations = np.empty((0, 2))
            for batch_size_index in range(0, len(train_batch), batch_size):
                current_batch = train_batch[batch_size_index:batch_size_index + batch_size]
                locs = sess.run(som_layer.getlocation(), feed_dict={x: current_batch})
                locations = np.append(locations, [locs])

            lenLoc = int(len(locations) / 2)
            locations = locations.reshape(lenLoc, 2).astype(int)

            # Update the data:
            self.trained = True
            saver.save(sess, 'model/som_model.ckpt')

            # The data will look like PCA dim 2 so it good to save to database as well.
            return locations, cluster_labels

    def save_model(self, file_path: str):
        dump(self, file_path)

    def semi_train_model(self, data, labels):
        pass

    def predict_model(self, predicted_data: ndarray):
        if not self.trained:
            raise Exception("the analyzer haven't trained yet, please train first")

        batch_size = self.batch_size

        el1 = FNN(10, 8, act=self.tf_atan, d_act=self.d_tf_atan)
        el2 = FNN(8, 6, act=self.tf_atan, d_act=self.d_tf_atan)
        el3 = FNN(6, 4, act=self.tf_atan, d_act=self.d_tf_atan)
        el4 = FNN(4, 2, act=self.tf_atan, d_act=self.d_tf_atan)
        som_layer = SOMLayer(self.map_width_height, self.map_width_height, self.map_dim,
                             num_epoch=self.num_epoch, learning_rate_som=0.8,
                             radius_factor=1.1, gaussian_std=0.08)

        # graph
        x = tf.placeholder(shape=[None, 10], dtype=tf.float32, name="input")
        current_iter = tf.placeholder(shape=[], dtype=tf.float32)

        saver = tf.train.Saver()

        # encoder
        elayer1 = el1.feedforward(x)
        elayer2 = el2.feedforward(elayer1)
        elayer3 = el3.feedforward(elayer2)
        elayer4 = el4.feedforward(elayer3)
        som_layer.feedforward(elayer4)

        som_update, som_grad = som_layer.backprop(current_iter, self.num_epoch)
        egrad4, egrad4_up = el4.backprop(som_grad)
        egrad3, egrad3_up = el3.backprop(egrad4)
        egrad2, egrad2_up = el2.backprop(egrad3)
        egrad1, egrad1_up = el1.backprop(egrad2)
        grad_update = som_update + egrad4_up + egrad3_up + egrad2_up + egrad1_up

        with tf.Session() as sess:
            saver.restore(sess, 'model/som_model.ckpt')
            trained_map = sess.run(som_layer.getmap()).reshape(self.map_width_height, self.map_width_height,
                                                               self.map_dim)

            # locations = sess.run(som_layer.getlocation(), feed_dict={x: predicted_data}).astype(int)
            # x1 = locations[:, 0]
            # y1 = locations[:, 1]

            # At the moment the predicted data contain first_comp and sec_comp
            x1 = predicted_data[:, 0]
            y1 = predicted_data[:, 1]
            map = trained_map[x1, y1]

            cluster_labels = self.model.predict(map)
            return cluster_labels

    def get_map_label(self):
        if not self.trained:
            raise Exception("the analyzer haven't trained yet, please train first")

        batch_size = self.batch_size

        el1 = FNN(10, 8, act=self.tf_atan, d_act=self.d_tf_atan)
        el2 = FNN(8, 6, act=self.tf_atan, d_act=self.d_tf_atan)
        el3 = FNN(6, 4, act=self.tf_atan, d_act=self.d_tf_atan)
        el4 = FNN(4, 2, act=self.tf_atan, d_act=self.d_tf_atan)
        som_layer = SOMLayer(self.map_width_height, self.map_width_height, self.map_dim,
                             num_epoch=self.num_epoch, learning_rate_som=0.8,
                             radius_factor=1.1, gaussian_std=0.08)

        # graph
        x = tf.placeholder(shape=[None, 10], dtype=tf.float32, name="input")
        current_iter = tf.placeholder(shape=[], dtype=tf.float32)

        saver = tf.train.Saver()

        # encoder
        elayer1 = el1.feedforward(x)
        elayer2 = el2.feedforward(elayer1)
        elayer3 = el3.feedforward(elayer2)
        elayer4 = el4.feedforward(elayer3)
        som_layer.feedforward(elayer4)

        som_update, som_grad = som_layer.backprop(current_iter, self.num_epoch)
        egrad4, egrad4_up = el4.backprop(som_grad)
        egrad3, egrad3_up = el3.backprop(egrad4)
        egrad2, egrad2_up = el2.backprop(egrad3)
        egrad1, egrad1_up = el1.backprop(egrad2)
        grad_update = som_update + egrad4_up + egrad3_up + egrad2_up + egrad1_up

        with tf.Session() as sess:
            saver.restore(sess, 'model/som_model.ckpt')
            trained_map = sess.run(som_layer.getmap()).reshape(self.map_width_height, self.map_width_height,
                                                               self.map_dim)

            predicted_data = np.array([[0, 0]])

            for i in range(0, 100):
                for j in range(0, 100):
                    if i == 0 and j == 0:
                        continue
                    predicted_data = np.append(predicted_data, [[i, j]], axis=0)
            x1 = predicted_data[:, 0]
            y1 = predicted_data[:, 1]
            map = trained_map[x1, y1]

            cluster_labels = self.model.predict(map)
            return cluster_labels

    @staticmethod
    def load_model(file_path: str) -> 'CarbotecSomAnalyzer':
        try:
            som_analyzer = load(file_path)
            return som_analyzer
        except:
            return None


"""

if __name__ == '__main__':
    trainning_data = pd.read_csv('data.csv')
    kmean_analyzer = CarbotecKmeansAnalyzer(6)

    cluster = kmean_analyzer.train_model(trainning_data)
    print(cluster[0:5])

    kmean_analyzer.save_model('kmean.pkl')

    another_test_data = numpy.array(
        [[64.62, 53.52, 42.9, 32.7, 22.2, 57.26, 0, 0, 0.62, 35.7, 45.52, 25.86, 16.76, 26, 99, 1]])

    new_kmean_analyzer = CarbotecKmeansAnalyzer.load_model('kmeans.pkl')

    if new_kmean_analyzer is not None:
        cluster2 = new_kmean_analyzer.predict_model(another_test_data)
        print(cluster2)

"""
