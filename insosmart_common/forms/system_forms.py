from django import forms

from insosmart_common.models import ProcessSystem


class SystemCreationForm(forms.ModelForm):
    """
    Game creation form to be rendered in template. The fields are given the constraints.
    """
    name = forms.CharField(required=True)
    description = forms.CharField(required=False, widget=forms.Textarea)
    system_type = forms.ChoiceField(choices=ProcessSystem.SYSTEM_TYPES, label="", initial=ProcessSystem.SYSTEM_CARBOTEC,
                                    widget=forms.Select(),
                                    required=True)

    def clean_name(self):
        # Validate if the name is unique or not
        name = self.cleaned_data.get('name')
        if name and ProcessSystem.objects.filter(name=name).exists():
            raise forms.ValidationError(u'Game name must be unique')
        return name

    class Meta:
        model = ProcessSystem
        fields = ('name', 'system_type', 'description')


class SystemEditForm(forms.ModelForm):
    """
    Game edit form with limited fields, the url and name field are not exposed in edit view.
    """

    description = forms.CharField(required=False, widget=forms.Textarea)

    class Meta:
        model = ProcessSystem
        fields = ('description',)
