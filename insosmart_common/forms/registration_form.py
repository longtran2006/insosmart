from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.db import transaction
from django import forms

from insosmart_common.models import Operator, Manager, UserProfile


class OperatorSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError(u'Email addresses must be unique.')
        return email

    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.save()
        try:
            group = Group.objects.get(name="operators")
            group.user_set.add(user)
        except Group.DoesNotExist:
            group = Group.objects.create(name="operators")
            group.user_set.add(user)

        user.save()

        player = Operator.objects.create(user=user)
        return user


class ManagerSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError(u'Email addresses must be unique.')
        return email

    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.save()
        try:
            group = Group.objects.get(name="managers")
            group.user_set.add(user)
        except Group.DoesNotExist:
            group = Group.objects.create(name="managers")
            group.user_set.add(user)

        user.save()
        developer = Manager.objects.create(user=user)
        return user


class UserProfileEditForm(forms.ModelForm):
    """
       USer profile edit form the url and name field are not exposed in edit view.
    """

    description = forms.CharField(required=False,
                                  widget=forms.TextInput(
                                      attrs={'class': 'form-control', 'placeholder': 'Something about you'}))
    city = forms.CharField(required=False,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'City'}))
    phone = forms.CharField(required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Phone'}))

    class Meta:
        model = UserProfile
        fields = ('description', 'city', 'phone')
