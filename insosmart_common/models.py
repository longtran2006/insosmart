from enum import Enum

from django.db import models


class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True, editable=False)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class MeasureType(Enum):
    # Type of data to be measured by sensors
    TEMPERATURE = "TEMPERATURE"
    HUMIDITY = "HUMIDITY"
    CARBON_DIOXIDE = "CARBON_DIOXIDE"
    PRESSURE = "PRESSURE"
    WATER_LEVEL = "WATER_LEVEL"

