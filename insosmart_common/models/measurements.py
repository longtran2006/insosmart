from django.db import models

from insosmart_common.models import ProcessSystem


class CarbotecMeasurement(models.Model):
    TiFurnace = models.FloatField()
    TiFlame = models.FloatField()
    TDryerHotAir = models.FloatField()
    TDryerInputAir = models.FloatField()
    TDryer1= models.FloatField()
    TDryer2 = models.FloatField()
    CO = models.FloatField()
    PAugerFront = models.FloatField()
    PAugerRear = models.FloatField()
    TCarbon = models.FloatField()
    TGas = models.FloatField()
    CarbonAugerSpeed = models.FloatField()
    FuelAugerSpeed= models.FloatField()
    BurnerValve= models.FloatField()
    FlareValve= models.FloatField()
    WorkingShift= models.IntegerField()
    DateTime = models.DateTimeField()
    label = models.IntegerField(null=True, blank=True)
    first_comp = models.FloatField(null= True, blank=True)
    second_comp = models.FloatField(null=True, blank=True)
    constraint_label = models.IntegerField(null=True, blank=True, default=-1)


    system = models.ForeignKey(
        to=ProcessSystem, on_delete=models.CASCADE, related_name='measurements', null=True)

    class Meta:
        app_label = 'insosmart_common'
        ordering = ['-id', ]