from django.db import models


class DeviceInterfaceSetting(models.Model):
    url = models.CharField(max_length=150)

    class Meta:
        app_label = 'insosmart_common'
        abstract = True


class MqttDeviceInterfaceSetting(DeviceInterfaceSetting):
    topic = models.CharField(max_length=150)

    class Meta:
        app_label = 'insosmart_common'
        ordering = ['-id']


class ApiDeviceInterfaceSetting(DeviceInterfaceSetting):
    token = models.CharField(max_length=150)

    class Meta:
        app_label = 'insosmart_common'
        ordering = ['-id']


class OpcUaDeviceInterfaceSetting(DeviceInterfaceSetting):
    node_structure = models.CharField(max_length=300)

    class Meta:
        app_label = 'insosmart_common'
        ordering = ['-id']
