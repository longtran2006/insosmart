from django.db import models

from insosmart_common.models import Manager, Operator
from insosmart_common.models.device_interfaces import MqttDeviceInterfaceSetting, OpcUaDeviceInterfaceSetting, \
    ApiDeviceInterfaceSetting


class BaseSystem(models.Model):
    INTERFACE_MQTT = "MQTT"
    INTERFACE_API = "HTTP"
    INTERFACE_OPCUA = "OPC-UA"

    MEASUREMENT_INTERFACES = (
        (INTERFACE_MQTT, INTERFACE_MQTT),
        (INTERFACE_OPCUA, INTERFACE_OPCUA),
        (INTERFACE_API, INTERFACE_API),
    )

    created_date = models.DateTimeField(auto_now_add=True, editable=False)
    modified_date = models.DateTimeField(auto_now=True)

    managers = models.ManyToManyField(to=Manager, related_name="systems", blank=True)
    operators = models.ManyToManyField(to=Operator, related_name="systems", blank=True)

    mqtt_setting = models.OneToOneField(to=MqttDeviceInterfaceSetting, related_name="system", blank=True, null=True,
                                        on_delete=models.CASCADE)
    api_setting = models.OneToOneField(to=ApiDeviceInterfaceSetting, related_name="system", blank=True, null=True,
                                       on_delete=models.CASCADE)
    opcua_setting = models.OneToOneField(to=OpcUaDeviceInterfaceSetting, related_name="system", blank=True, null=True,
                                         on_delete=models.CASCADE)

    class Meta:
        app_label = 'insosmart_common'
        abstract = True


class ProcessSystem(BaseSystem):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField()

    SYSTEM_CARBOTEC = "Carbotec"

    SYSTEM_TYPES = (
        (SYSTEM_CARBOTEC, SYSTEM_CARBOTEC),
    )

    MODEL_UNKNOWN = "Unknown"
    MODEL_SOM_KMEAN = "SomKmeanModel"
    MODEL_IVIS_KMEAN = "IvisKmeanModel"
    MODEL_PCA_KMEAN = "PcaKMeanModel"

    MODEL_TYPES = (
        (MODEL_UNKNOWN, MODEL_UNKNOWN),
        (MODEL_SOM_KMEAN, MODEL_SOM_KMEAN),
        (MODEL_IVIS_KMEAN, MODEL_IVIS_KMEAN),
        (MODEL_PCA_KMEAN, MODEL_PCA_KMEAN),
    )

    system_type = models.CharField(choices=SYSTEM_TYPES, default=SYSTEM_CARBOTEC, max_length=255)

    trainned_date = models.DateTimeField(blank=True, null=True)
    is_trained = models.BooleanField(blank=True, null=True, default=False)
    is_imported = models.BooleanField(blank=True, null=True, default=False)

    model_type = models.CharField(choices=MODEL_TYPES, default=MODEL_UNKNOWN, max_length=255)

    class Meta:
        app_label = 'insosmart_common'
        ordering = ['-id']
