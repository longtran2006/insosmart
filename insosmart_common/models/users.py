"""
All models, methods related to users (admin, operator, owner) are implemented here
Admin has the right to observe, modify the system, and add or remove other users
Operator can observe, modify the system and receive notifications on emergency situations
Owner has the right to choose the number of control units and observe the general circumstance of the system
"""

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Listen to the user creation event to create the token related to the user.
    The Token model is come from
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    if created:
        Token.objects.create(user=instance)


class Manager(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        app_label = "insosmart_common"


class Operator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        app_label = "insosmart_common"


class Owner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        app_label = "insosmart_common"


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=100, default='')
    country = models.CharField(max_length=100, default='')
    city = models.CharField(max_length=100, default='')
    phone = models.CharField(max_length=10, default='')
    company = models.CharField(max_length=100, default='')
    email_address = models.EmailField()

    # require Meta class
    class Meta:
        app_label = "insosmart_common"


# listen event from User creation to create an user profile
def create_profile(sender, **kwargs):
    if (kwargs['created']):
        user_profile = UserProfile.objects.create(user=kwargs['instance'])


# Send event when creating user
post_save.connect(create_profile, sender=User)
