from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test


def operator_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='login'):
    """
    Decirator that checks that the user logged in is an operator. If not redirect to the login page
    :param function:
    :param redirect_field_name:
    :param login_url: login url built-in of django auth
    :return:
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.groups.filter(name='operators').exists(),
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )

    if function:
        return actual_decorator(function)
    return actual_decorator


def manager_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='login'):
    """
    Decorator that checks the user logged in is a manager. If not redirect to the login page
    :param function:
    :param redirect_field_name:
    :param login_url:
    :return:
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.groups.filter(name='managers').exists(),
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )

    if function:
        return actual_decorator(function)
    return actual_decorator
