from rest_framework import permissions


class IsManager(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.buyer == request.user and request.method in permissions.SAFE_METHODS


class IsAdmin(permissions.AllowAny):
    def has_permission(self, request, view):
        return request.user.groups.filter(name='admins').exists()


class IsOperator(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.filter(name='operators').exists()