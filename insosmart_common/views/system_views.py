import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView

from insosmart_common.decorators import manager_required
from insosmart_common.forms.system_forms import SystemCreationForm, SystemEditForm
from insosmart_common.models import ProcessSystem, CarbotecMeasurement
from insosmart_common.tasks.carbotec_tasks import train_carbotec_data_task, import_data_from_csv
from insosmart_statistics.plots import carbotec_statistic_plots


@method_decorator([login_required(login_url='/user/login'), manager_required(login_url='/user/login')],
                  name='dispatch')
class SystemListView(ListView):
    """
    A view that list all of the game
    """
    model = ProcessSystem
    template_name = 'system/manage_systems.html'
    context_object_name = 'system_list'
    paginate_by = 15

    def get_queryset(self):
        manager_id = self.request.user.manager.pk
        process_systems = ProcessSystem.objects.filter(managers__pk=manager_id).all()
        return process_systems


@method_decorator([login_required(login_url='/user/login'), manager_required], name='dispatch')
class SystemCreateView(CreateView):
    """
    A view to create game
    """
    model = ProcessSystem
    form_class = SystemCreationForm
    template_name = 'system/add_system.html'

    def form_valid(self, form):
        # create system
        system = form.save(commit=False)
        system.save()
        # add user to the system
        system.managers.add(self.request.user.manager)
        messages.success(self.request, 'You added a system successfully')

        # return redirect('edit_game', system.pk)
        return redirect('manage_systems')


@method_decorator([login_required(login_url='/user/login'), manager_required], name='dispatch')
class SystemEditView(UpdateView):
    model = ProcessSystem
    form_class = SystemEditForm
    context_object_name = 'system'
    template_name = 'system/edit_system.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        '''
        Auto match with the id of the game
        '''
        return self.request.user.manager.systems.all()

    def form_valid(self, form):
        system = form.save(commit=False)

        system.save()
        messages.success(self.request, 'You added a system successfully')

        return redirect('manage_systems')

    def get_success_url(self):
        # return reverse('edit_game', kwargs={'pk': self.object.pk})
        return reverse('manage_systems')


@method_decorator([login_required(login_url='/user/login'), manager_required()], name='dispatch')
class SystemDeleteView(DeleteView):
    """
    A view to delete the game
    """
    model = ProcessSystem
    context_object_name = 'system'
    template_name = 'system/delete_system.html'
    success_url = reverse_lazy('manage_systems')

    def delete(self, request, *args, **kwargs):
        system = self.get_object()
        messages.success(request, 'The system %s was deleted with success!' % system.name)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.manager.systems.all()


@method_decorator([login_required(login_url='/user/login'), manager_required], name='dispatch')
class SystemDetailView(DetailView):
    """
    A view to get the detail data of the game
    """
    model = ProcessSystem
    template_name = 'system/detail_system.html'
    context_object_name = 'system'

    def get_context_data(self, **kwargs):
        """
        Trying to get more data to present in template:
        """
        systemid = self.kwargs['pk']
        name_map = {'count': 'measurement_count'}
        counts = CarbotecMeasurement.objects.raw(
            'SELECT 1 as id, COUNT(*) as measurement_count from insosmart_common_carbotecmeasurement '
            'WHERE system_id = %s', [systemid], translations=name_map)

        extra_context = {
            'measurement_count': counts[0].measurement_count,
            'plot' : carbotec_statistic_plots.get_carbotec_heatmap_plot()
        }
        kwargs.update(extra_context)
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return ProcessSystem.objects.all()


@csrf_exempt
@login_required(login_url='/user/login')
@manager_required()
def train_system(request, pk):
    if request.method == "POST":
        train_carbotec_data_task.delay(pk)

        json_data = json.dumps("OK")
        messages.success(request, 'The system is training!')
        return HttpResponse(json_data, content_type="application/json")


@csrf_exempt
@login_required(login_url='/user/login')
@manager_required()
def import_system_data(request, pk):
    if request.method == "POST":
        import_data_from_csv.delay(pk)

        json_data = json.dumps("OK")
        messages.success(request, 'The system data is importing!')
        return HttpResponse(json_data, content_type="application/json")
