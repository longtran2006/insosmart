from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import FormView, CreateView
from django.views.generic.base import TemplateView

# from gamestore.forms.registration_form import UserProfileEditForm
from insosmart_common.forms.registration_form import OperatorSignUpForm, ManagerSignUpForm
from insosmart_common.models.users import UserProfile


# from gamestore.tokens import account_activation_token


@method_decorator([login_required(login_url='/user/login'), ], name='dispatch')
class UserProfileDetailView(TemplateView):
    model = UserProfile
    template_name = 'user/profile.html'

    def get_context_data(self, **kwargs):
        context = super(UserProfileDetailView, self).get_context_data(**kwargs)
        # context['form'] = UserProfileEditForm
        return context


@method_decorator([login_required(login_url='/user/login'), ], name='dispatch')
class UserProfileEditView(FormView):
    # form_class = UserProfileEditForm
    success_url = reverse_lazy('view_profile')

    def form_valid(self, form):
        profile = self.request.user.userprofile
        profile.country = form.cleaned_data['country']
        profile.city = form.cleaned_data['city']
        profile.description = form.cleaned_data['description']
        profile.phone = form.cleaned_data['phone']
        profile.save()
        messages.success(self.request, 'You updated it successfully')
        return redirect('view_profile')


def do_registration(request):
    return render(request, 'user/signup.html')


def activated(request):
    return render(request, 'user/acc_activation_done.html')




@login_required()
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'user/change_password.html', {
        'form': form
    })


class OperatorRegistrationView(CreateView):
    model = User
    form_class = OperatorSignUpForm
    template_name = 'user/signup_operator_form.html'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = True
        user.save()

        return redirect('activated')


class ManagerRegistrationView(CreateView):
    model = User
    form_class = ManagerSignUpForm
    template_name = 'user/signup_manager_form.html'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = True
        user.save()

        return redirect('activated')
