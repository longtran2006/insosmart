from django.apps import AppConfig


class InsosmartCommonConfig(AppConfig):
    name = 'insosmart_common'
