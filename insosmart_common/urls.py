from django.urls import path, include
from django.contrib.auth import views as auth_views
from insosmart_common.views import user_views, system_views

urlpatterns = [
    path('user/', include('django.contrib.auth.urls')),
    path('user/login', auth_views.LoginView.as_view(template_name='user/login.html'), name='login'),
    path('user/view_profile', user_views.UserProfileDetailView.as_view(), name='view_profile'),
    path('user/edit_profile/', user_views.UserProfileEditView.as_view(), name='edit_profile'),
    path('user/signup', user_views.do_registration, name='registration'),
    path('user/signup/operator/', user_views.OperatorRegistrationView.as_view(), name='operator_registration'),
    path('user/signup/manager/', user_views.ManagerRegistrationView.as_view(), name='manager_registration'),
    path('user/activated', user_views.activated, name='activated'),
    path('user/change_password', user_views.change_password, name='change_password'),

    path('system/manage_systems', system_views.SystemListView.as_view(), name='manage_systems'),
    path('system/create_system', system_views.SystemCreateView.as_view(), name='add_system'),

    path('system/<int:pk>/edit', system_views.SystemEditView.as_view(), name='edit_system'),
    path('system/<int:pk>/delete', system_views.SystemDeleteView.as_view(), name='remove_system'),
    path('system/<int:pk>/detail', system_views.SystemDetailView.as_view(), name='detail_system'),
    path('system/<int:pk>/train', system_views.train_system, name='train_system'),
    path('system/<int:pk>/import', system_views.import_system_data, name='import_system_data'),

]
