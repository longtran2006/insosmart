from django.contrib import admin

# Register your models here.
from insosmart_common.models import ProcessSystem, CarbotecMeasurement, MqttDeviceInterfaceSetting, \
    OpcUaDeviceInterfaceSetting, ApiDeviceInterfaceSetting
from insosmart_common.models.users import Manager, Owner, Operator

admin.site.register(Manager)
admin.site.register(Operator)
admin.site.register(Owner)
admin.site.register(ProcessSystem)
admin.site.register(CarbotecMeasurement)
admin.site.register(MqttDeviceInterfaceSetting)
admin.site.register(ApiDeviceInterfaceSetting)
admin.site.register(OpcUaDeviceInterfaceSetting)
