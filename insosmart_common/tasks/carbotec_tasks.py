from asgiref.sync import async_to_sync
from celery import shared_task
from datetime import datetime

from channels.layers import get_channel_layer
from django.db import transaction, connection
import pandas as pd
import numpy as np
from insosmart_common.models import CarbotecMeasurement, ProcessSystem
from insosmart_ml.carbotec.CarbotecAnalyzer import CarbotecKmeansAnalyzer, CarbotecSomAnalyzer, CarbotecIvisAnalyzer, \
    CarbotecConstrainedKmeansAnalyzer
from sklearn.decomposition import PCA

channel_layer = get_channel_layer()


@shared_task
def import_data_from_csv(system_id):
    print("Import carbotex csv file to CarbotecModel")
    data = pd.read_csv('GroupData.csv')

    system = ProcessSystem.objects.get(pk=system_id)
    if system is None:
        reportProcess("import_data_from_csv", "system not found", system_id)
        return

    print("Delete all old data")
    cursor = connection.cursor()
    cursor.execute("DELETE FROM insosmart_common_carbotecmeasurement WHERE system_id = %s", [system_id])

    system.is_imported = False
    system.is_trained = False
    system.save()

    reportProcess("import_data_from_csv", "data is loaded", system_id)

    model_instances = [CarbotecMeasurement(
        TiFurnace=float(record['TI-Furnace']),
        TiFlame=float(record['TI-Flame']),
        TDryerHotAir=float(record['T-DryerHotAir']),
        TDryerInputAir=float(record['T-DryerInputAir']),
        TDryer1=float(record['T-Dryer1']),
        TDryer2=float(record['T-Dryer2']),
        CO=float(record['CO']),
        PAugerFront=float(record['P-AugerFront']),
        PAugerRear=float(record['P-AugerRear']),
        TCarbon=float(record['T-carbon']),
        TGas=float(record['T-gas']),
        CarbonAugerSpeed=float(record['CarbonAugerSpeed']),
        FuelAugerSpeed=float(record['FuelAugerSpeed']),
        BurnerValve=float(record['BurnerValve']),
        FlareValve=float(record['FlareValve']),
        WorkingShift=int(record['WorkingShift']),
        DateTime=datetime.strptime(record['DateTime'], '%Y-%m-%d %H:%M:%S.%f'),
        system=system,
        label=-1,
        constraint_label=-1
    ) for index, record in data.iterrows()]

    CarbotecMeasurement.objects.bulk_create(model_instances)

    system.is_imported = True
    system.model_type = ProcessSystem.MODEL_UNKNOWN
    system.save()

    print("Save carbotex")
    reportProcess("import_data_from_csv", "data is saved", system_id)


def validateAndRetriveData(system_id):
    print("Retrieving data to dataframe")

    system = ProcessSystem.objects.get(pk=system_id)
    if system is None:
        reportProcess("train_carbotec_data_task", "System not found", system_id)
        return False, None, None

    system.is_trained = False
    system.save()

    data = CarbotecMeasurement.objects.filter(system__id=system_id).values()
    if data.count() == 0:
        reportProcess("train_carbotec_data_task", "System does not have data", system_id)
        return False, None, None

    reportProcess("train_carbotec_data_task", "Start progress", system_id)

    print("Converting data to dataframe")
    data_to_train = pd.DataFrame.from_records(data, exclude=[
        'DateTime', 'label', 'constraint_label', 'first_comp', 'second_comp', 'system_id', 'id'
    ])

    data_to_train = data_to_train.drop(
        ['TDryerHotAir', 'TDryerInputAir', 'CarbonAugerSpeed', 'FuelAugerSpeed', 'WorkingShift', 'PAugerFront'], axis=1)

    print("retrieved all data")
    reportProcess("train_carbotec_data_task", "Data retrieved", system_id)
    return True, system, data_to_train


def validateAndRetrieveDataConstraintLabels(system_id):
    print("Retrieving data to dataframe")

    system = ProcessSystem.objects.get(pk=system_id)
    if system is None:
        reportProcess("train_carbotec_data_task", "System not found", system_id)
        return False, None, None

    system.is_trained = False
    system.save()

    data = CarbotecMeasurement.objects.filter(system__id=system_id).values()
    if data.count() == 0:
        reportProcess("train_carbotec_data_task", "System does not have data", system_id)
        return False, None, None

    reportProcess("train_carbotec_data_task", "Start progress", system_id)

    print("Converting data to dataframe")
    data_to_train = pd.DataFrame.from_records(data, exclude=[
        'DateTime', 'first_comp', 'second_comp', 'system_id', 'id', 'label'
    ])

    labels = data_to_train['constraint_label']

    data_to_train = data_to_train.drop(
        ['TDryerHotAir', 'TDryerInputAir', 'CarbonAugerSpeed',
         'FuelAugerSpeed', 'WorkingShift', 'PAugerFront', 'constraint_label'], axis=1)

    print("retrieved all data")
    reportProcess("train_carbotec_data_task", "Data retrieved", system_id)
    return True, system, data_to_train, labels


@shared_task
def train_carbotec_data_task(system_id):
    valid, system, data_to_train = validateAndRetriveData(system_id)
    if valid is not True:
        reportProcess("train_carbotec_data_task", "Data is not trained", system_id)
        return

    kmean_analyzer = CarbotecKmeansAnalyzer(6)
    kmean_analyzer.correlations = data_to_train.corr()

    pca = PCA(n_components=2)
    pca_result = pca.fit_transform(data_to_train.values)

    cluster = kmean_analyzer.train_model(pca_result, False)
    print("converted and trained data" + str(cluster.size))

    kmean_analyzer.save_model('kmean.pkl')

    # using transaction for reducing multiple single update
    with transaction.atomic():
        amount = cluster.size
        print(amount)
        i = 0
        for group in CarbotecMeasurement.objects.filter(system__id=system_id)[:amount].iterator():
            group.label = cluster[i]
            group.first_comp = pca_result[i][0]
            group.second_comp = pca_result[i][1]
            group.save()
            i = i + 1

    system.trained_date = datetime.now()
    system.is_trained = True
    system.model_type = ProcessSystem.MODEL_PCA_KMEAN
    system.save()

    print("Saved labels")
    reportProcess("train_carbotec_data_task", "Data is trained", system_id)


@shared_task
def train_carbotec_data_task_semi(system_id):
    valid, system, data_to_train, labels = validateAndRetrieveDataConstraintLabels(system_id)
    if valid is not True:
        reportProcess("train_carbotec_data_task", "Data is not trained", system_id)
        return

    kmean_analyzer = CarbotecConstrainedKmeansAnalyzer(6)

    pca = PCA(n_components=2)
    pca_result = pca.fit_transform(data_to_train.values)

    cluster = kmean_analyzer.semi_train_model(pca_result, labels, False)
    print("converted and trained data" + str(cluster.size))

    kmean_analyzer.save_model('constraintKmean.pkl')

    # using transaction for reducing multiple single update
    with transaction.atomic():
        amount = cluster.size
        print(amount)
        i = 0
        for group in CarbotecMeasurement.objects.filter(system__id=system_id)[:amount].iterator():
            group.label = cluster[i]
            group.first_comp = pca_result[i][0]
            group.second_comp = pca_result[i][1]
            group.save()
            i = i + 1

    system.trained_date = datetime.now()
    system.is_trained = True
    system.model_type = ProcessSystem.MODEL_PCA_KMEAN
    system.save()

    print("Saved labels")
    reportProcess("train_carbotec_data_task", "Data is trained", system_id)


@shared_task()
def train_carbotec_data_task_som(system_id):
    print("Som: Retrieving data to dataframe")

    valid, system, data_to_train = validateAndRetriveData(system_id)
    if valid is not True:
        reportProcess("train_carbotec_data_task", "Data is not trained", system_id)
        return

    som_analyzer = CarbotecSomAnalyzer(6)
    location, cluster = som_analyzer.train_model(data_to_train.values, True)
    print("Som: converted and trained data")

    som_analyzer.save_model('som.pkl')
    print("Som: update the cluster labels")
    with transaction.atomic():
        amount = cluster.size
        print(amount)
        i = 0
        for group in CarbotecMeasurement.objects.filter(system__id=system_id)[:].iterator():
            group.first_comp = location[i][0]
            group.second_comp = location[i][1]
            group.label = cluster[location[i][0], location[i][1]]
            group.save()
            i = i + 1

    system.trained_date = datetime.now()
    system.is_trained = True
    system.model_type = ProcessSystem.MODEL_SOM_KMEAN
    system.save()

    print("Som Saved labels")
    reportProcess("train_carbotec_data_task", "Data is trained", system_id)


@shared_task()
def train_carbotec_data_task_ivis(system_id):
    print("Ivis: Retrieving data to dataframe")

    valid, system, data_to_train = validateAndRetriveData(system_id)
    if valid is not True:
        reportProcess("train_carbotec_data_task", "Data is not trained", system_id)
        return

    ivis_analyzer = CarbotecIvisAnalyzer(6)
    ivis_data, cluster = ivis_analyzer.train_model(data_to_train.values, True)
    print("Ivis: converted and trained data")

    ivis_analyzer.save_model('ivis.pkl')
    print("Ivis: update the cluster labels")
    with transaction.atomic():
        amount = cluster.size
        print(amount)
        i = 0
        for group in CarbotecMeasurement.objects.filter(system__id=system_id)[:amount].iterator():
            group.label = cluster[i]
            group.first_comp = ivis_data[i][0]
            group.second_comp = ivis_data[i][1]
            group.save()
            i = i + 1

    system.trained_date = datetime.now()
    system.is_trained = True
    system.model_type = ProcessSystem.MODEL_IVIS_KMEAN
    system.save()

    print("Ivis Saved labels")
    reportProcess("train_carbotec_data_task", "Data is trained", system_id)


@shared_task
def test_carbotec_data_task(system_id):
    print("Saved labels")
    reportProcess("train_carbotec_data_task", "Data is trained", system_id)


def reportProcess(topic, progress, system_id):
    async_to_sync(channel_layer.group_send)(
        "systems", {
            "type": "group.message",
            "topic": topic,
            "result": {
                "systemId": system_id,
                "progress": progress
            }
        }
    )

