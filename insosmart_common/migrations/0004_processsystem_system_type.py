# Generated by Django 2.1.5 on 2019-04-27 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insosmart_common', '0003_auto_20190426_0054'),
    ]

    operations = [
        migrations.AddField(
            model_name='processsystem',
            name='system_type',
            field=models.CharField(choices=[('Carbotec', 'Carbotec')], default='Carbotec', max_length=255),
        ),
    ]
