from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path

import insosmart_api
import insosmart_statistics

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(
        URLRouter(
            [
                path("ws/statistics/", insosmart_statistics.consumers.StatisticConsumer),
                path("ws/systems/", insosmart_api.consumers.SystemConsumer),
            ]
        )
    ),
})
