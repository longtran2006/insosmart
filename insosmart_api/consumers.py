import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class SystemConsumer(WebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.group_name = "systems"

    def connect(self):
        # Join statistics group
        async_to_sync(self.channel_layer.group_add)(self.group_name, self.channel_name)

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(self.group_name, self.channel_name)

    def receive(self, text_data):
        data_json = json.loads(text_data)
        topic = data_json['topic']
        param = data_json['data']

        print("received from somewhere")

        if topic == 'import_system_data':
            # Send message to room group
            async_to_sync(self.channel_layer.group_send)(
                self.group_name,
                {
                    'type': topic,
                    'param': param
                }
            )

    def group_message(self, result):
        print("Receive message from a source")
        # Send message to WebSocket
        topic = result['topic']
        data = result['result']
        self.send(text_data=json.dumps({
            'topic': topic,
            'data': data,
        }))

    def import_system_data(self, event):
        # Receive message from room group
        param = event['param']
        print(param)

        system_id = param['systemId']
        self.send(text_data=json.dumps({
            'text': system_id
        }))