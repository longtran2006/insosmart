import json

import numpy
from channels.layers import get_channel_layer
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.expressions import F
from django.http.response import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.decorators import authentication_classes, permission_classes, api_view
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from insosmart_api.systems.system_serializers import SystemSerializer, SystemUpdateSerializer
from insosmart_common.models import ProcessSystem, CarbotecMeasurement
from insosmart_common.permissions import IsManager
from insosmart_common.tasks import train_carbotec_data_task, train_carbotec_data_task_som, import_data_from_csv, \
    test_carbotec_data_task, \
    CarbotecKmeansAnalyzer, CarbotecSomAnalyzer, train_carbotec_data_task_ivis, train_carbotec_data_task_semi
import pandas as pd

channel_layer = get_channel_layer()

@csrf_exempt
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def system_manager_list(request):
    if request.method == 'GET':
        manager_id = request.user.manager.pk

        process_systems = ProcessSystem.objects.filter(managers__pk=manager_id).all()
        serializer = SystemSerializer(process_systems, many=True)
        return JsonResponse(serializer.data, safe=False)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def system_detail(request, pk):
    if request.method == 'GET':
        print(pk)
        manager_id = request.user.manager.pk
        system = ProcessSystem.objects.filter(pk=pk, managers__pk=manager_id)[0]

        if system is None:
            return HttpResponse(status=404)

        serializer = SystemSerializer(system)
        return JsonResponse(serializer.data, safe=False)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def system_create(request):
    if request.method == 'POST':
        # print(request.data)
        data = JSONParser().parse(request)
        print(data)
        serializer = SystemSerializer(data=data)
        if serializer.is_valid():
            system = serializer.save()
            system.managers.add(request.user.manager)
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['PUT'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def system_edit(request, pk):
    if request.method == 'PUT':

        manager_id = request.user.manager.pk
        system = ProcessSystem.objects.filter(pk=pk, managers__pk=manager_id)[0]

        if system is None:
            return HttpResponse(status=404)

        serializer = SystemUpdateSerializer(system, data=request.data)
        if serializer.is_valid():
            serializer.update(system, serializer.validated_data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['DELETE'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def system_delete(request, pk):
    if request.method == 'DELETE':

        manager_id = request.user.manager.pk
        system = ProcessSystem.objects.filter(pk=pk, managers__pk=manager_id)[0]

        if system is None:
            return HttpResponse(status=404)

        systemId = system.id
        system.delete()
        """
        async_to_sync(channel_layer.group_send)(
            "systems", {"type": "system.message", "message": "Hello"}
        )
    
        """
        return Response(systemId, status=status.HTTP_204_NO_CONTENT)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def train_system(request, pk):
    if request.method == "POST":

        data = JSONParser().parse(request)
        train_type = int(data.get('train_type', 0))
        print(train_type)

        type = train_type

        if type == 1:
            print("start train with PCA and Kmeans")
            train_carbotec_data_task_semi.delay(pk)
        elif type == 2:
            print("start train with SOM and Kmeans")
            train_carbotec_data_task_som.delay(pk)
        elif type == 3:
            print("start train with IVIS and Kmeans")
            train_carbotec_data_task_ivis.delay(pk)
        else:
            print("did not match any type?")
            json_data = json.dumps("NOK")
            return HttpResponse(json_data, content_type="application/json")

        json_data = json.dumps("OK")
        return HttpResponse(json_data, content_type="application/json")


@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def import_system_data(request, pk):
    if request.method == "POST":
        import_data_from_csv.delay(pk)

        json_data = json.dumps("OK")
        return HttpResponse(json_data, content_type="application/json")


@csrf_exempt
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def get_plot_data(request, pk):

    system = ProcessSystem.objects.get(pk=pk)
    if system is None:
        return

    data = CarbotecMeasurement.objects.filter(system__pk=pk) \
        .annotate(idmod4=F('id') % 40) \
        .filter(idmod4=0) \
        .values('label', 'first_comp', 'second_comp')

    print(len(data))
    trained_data = pd.DataFrame.from_records(data)

    x = trained_data['first_comp'].values.tolist()
    y = trained_data['second_comp'].values.tolist()
    labels = trained_data['label'].values

    print(labels.min())
    print(labels.max())

    colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
                 3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'}
    cols = trained_data['label'].map(colorsIdx).tolist()
    json_data = json.dumps(dict(
        x=x,
        y=y,
        cols=cols,
        model=system.model_type
    ))

    return HttpResponse(json_data, content_type="application/json")



@csrf_exempt
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def get_som_plot_data(request, pk):
    system = ProcessSystem.objects.get(pk=pk)
    if system is None:
        return

    predicted_data = numpy.array([[0, 0]])

    for i in range(0,100):
        for j in range(0,100):
            if i == 0 and j == 0:
                continue
            predicted_data = numpy.append(predicted_data, [[i,j]], axis=0)

    print('predicted size ', predicted_data.size)
    model = CarbotecSomAnalyzer.load_model('som.pkl')

    result = model.get_map_label()
    colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
                 3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'}
    dataset = pd.DataFrame({'label': result})
    cols = dataset['label'].map(colorsIdx).tolist()

    x = predicted_data[:,0].tolist()
    y = predicted_data[:,1].tolist()

    json_data = json.dumps(dict(
        x=x,
        y=y,
        cols=cols
    ))

    return HttpResponse(json_data, content_type="application/json")

@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def get_regression_plot_data(request, pk):
    system = ProcessSystem.objects.get(pk=pk)
    if system is None:
        return

    data = system.measurements\
        .annotate(mod=F('id') % 60).filter(mod=True)\
        .values();
    data_to_show = pd.DataFrame.from_records(data, exclude=['first_comp', 'second_comp', 'system_id', 'id'])

    time = data_to_show['DateTime'].dt.strftime('%Y-%m-%d %H:%M:%S').values.tolist()
    furnace = data_to_show['TiFurnace'].values.tolist()
    augerSpeed= data_to_show['CarbonAugerSpeed'].values.tolist()

    colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
                 3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'}
    cols = data_to_show['label'].map(colorsIdx).tolist()

    json_data = json.dumps(dict(
        time=time,
        furnace=furnace,
        augerSpeed=augerSpeed,
        cols=cols,
        model=system.model_type
    ))

    return HttpResponse(json_data, content_type="application/json")

@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def get_predicted_data(request, pk):
    if request.method == "POST":

        data = JSONParser().parse(request)
        first_comp = data.get('first_comp', None)
        second_comp = data.get('second_comp', None)

        #model = CarbotecKmeansAnalyzer.load_model('kmean.pkl')
        model = CarbotecSomAnalyzer.load_model('som.pkl')
        predict_data = numpy.array(
            [[first_comp, second_comp]])

        result = list(model.predict_model(predict_data))
        colorsIdx = {0: 'rgb(0,0,0)', 1: 'rgb(66, 134, 244)', 2: 'rgb(151, 161, 178)',
                     3: 'rgb(188, 59, 20)', 4: 'rgb(181, 193, 13)', 5: 'rgb(15, 193, 149)'}
        cols = map(colorsIdx, result)

        color = 'rgb(0,0,0)'
        if result[0] == 1:
            color = colorsIdx[1]
        elif result[0] == 2:
            color = colorsIdx[2]
        elif result[0] == 3:
            color = colorsIdx[3]
        elif result[0] == 4:
            color = colorsIdx[4]
        elif result[0] == 5:
            color = colorsIdx[5]

        json_data = json.dumps(dict(
            x=[first_comp],
            y=[second_comp],
            cols=[color]
        ))

        print(json_data);

        return HttpResponse(json_data, content_type="application/json")
    else:
        return HttpResponseBadRequest()


@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def get_detail_measured_data(request, pk):
    if request.method == "POST":

        data = JSONParser().parse(request)
        first_comp = data.get('first_comp', None)
        second_comp = data.get('second_comp', None)
        label = data.get('label', -1)
        if first_comp is None or second_comp is None or label == -1:
            return HttpResponseBadRequest()

        measurement = CarbotecMeasurement.objects.filter(system__pk=pk, label=label,
                                                         first_comp__range=(first_comp - 0.01, first_comp + 0.01),
                                                         second_comp__range=(
                                                             second_comp - 0.01, second_comp + 0.01)).values()[0]
        if measurement is None:
            return HttpResponse(status=404)

        json_data = json.dumps(measurement,
                               sort_keys=True,
                               indent=1,
                               cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type="application/json")
    else:
        return HttpResponseBadRequest()


@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated, IsManager,))
def update_label_measured_data(request, pk):
    if request.method == "POST":
        data = JSONParser().parse(request)
        measurement_id = data.get('measurement_id', None)
        new_label = data.get('new_label', -1)

        measurement = CarbotecMeasurement.objects.filter(system__pk=pk, pk=measurement_id)[0]
        if measurement is None:
            return HttpResponse(status=404)

        measurement.label = new_label
        measurement.constraint_label = new_label
        measurement.save()

        data = serializers.serialize('json', [measurement, ])
        struct = json.loads(data)
        json_data = json.dumps(struct[0],
                               sort_keys=True,
                               indent=1,
                               cls=DjangoJSONEncoder)
        return HttpResponse(json_data, content_type="application/json")
    else:
        return HttpResponseBadRequest()
