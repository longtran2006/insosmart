"""
All game related serializations for the API are implemented here
"""

from rest_framework import serializers

from insosmart_common.models import ProcessSystem, CarbotecMeasurement


class SystemSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        system = ProcessSystem(
            name=validated_data.get('name'),
            description=validated_data.get('description', None),
            system_type=validated_data.get('system_type'),
            # trainned_date=validated_data.get('trainned_date', None),
            # modified_data=validated_data.get('modified_data', None),
            # created_date=validated_data.get('created_date', None),
        )
        system.save()
        return system

    class Meta:
        model = ProcessSystem
        fields = ('id', 'name', 'description', 'system_type', 'trainned_date', 'modified_date', 'created_date',
                  'is_trained', 'is_imported')
        # fields = ('id', 'name', 'description', 'system_type')


class SystemUpdateSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField()
    system_type = serializers.ReadOnlyField()

    def update(self, instance, validated_data):
        for field in validated_data:
            if field != "name" and field != "system_type":
                instance.__setattr__(field, validated_data.get(field))

        instance.save()
        return instance

    class Meta:
        model = ProcessSystem
        fields = ('id', 'name', 'description', 'system_type')
