from django.urls import path

from insosmart_api import consumers

websocket_urlpatterns = [
    path("ws/apis/systems", consumers.SystemConsumer)
]