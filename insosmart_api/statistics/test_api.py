from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response




@csrf_exempt
@api_view(['POST'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
def train_model(request):

    if request.method == 'POST':

        return JsonResponse("ABC", status=400)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@csrf_exempt
@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
def say_hello(request):

    if request.method == 'GET':
        return JsonResponse("Hello", safe=False)

    return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
