from django.urls import path
from django.urls.conf import include
from rest_framework_swagger.views import get_swagger_view

from insosmart_api.authentications import auth_api
from insosmart_api.statistics import test_api
from insosmart_api.systems import system_api

schema_view = get_swagger_view(title='Insosmart API')

urlpatterns = [
    path('api', schema_view, name='api'),
    path('say_hello', test_api.say_hello, name='say_hello'),
    # path('api/login', auth_api.login, name='api_login'),
    path('auth/', include('rest_auth.urls')),
    path('auth/registration/', include('rest_auth.registration.urls')),

    path('systems/getAll', system_api.system_manager_list, name='get_all_system_api'),
    path('systems/create', system_api.system_create, name='create_system_api'),
    path('systems/detail/<int:pk>/', system_api.system_detail, name='detail_system_api'),
    path('systems/edit/<int:pk>/', system_api.system_edit, name='edit_system_api'),
    path('systems/delete/<int:pk>/', system_api.system_delete, name='delete_system_api'),
    path('systems/trainData/<int:pk>/', system_api.train_system, name='train_system_api'),
    path('systems/importData/<int:pk>/', system_api.import_system_data, name='import_system_api'),
    path('systems/getClusterData/<int:pk>/', system_api.get_plot_data, name='get_cluster_data_api'),
    path('systems/getPredictedData/<int:pk>/', system_api.get_predicted_data, name='get_predicted_data_api'),
    path('systems/getMeasurementData/<int:pk>/', system_api.get_detail_measured_data, name='get_measured_data_api'),
    path('systems/updateMeasurementData/<int:pk>/', system_api.update_label_measured_data,
         name='update_label_measured_data_api'),
    path('systems/getSomClusterData/<int:pk>/', system_api.get_som_plot_data, name='get_som_cluster_data_api'),
    path('systems/getRegressionData/<int:pk>/', system_api.get_regression_plot_data, name='get_regression_data_api')
]
