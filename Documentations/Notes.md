* Autoencoder
    
    - https://medium.com/analytics-vidhya/journey-from-principle-component-analysis-to-autoencoders-e60d066f191a
    - https://towardsdatascience.com/pca-vs-autoencoders-1ba08362f450

* Data reduction:
    
    - https://www.analyticsvidhya.com/blog/2018/08/dimensionality-reduction-techniques-python/?fbclid=IwAR15RnMqrPn50RZYOqlePQHuDHVSSCSYsp5a-zBnQ2WetMMmSiL1ZnbijKk

* Data visualization and scoring:
    - https://plot.ly/scikit-learn/plot-kmeans-digits/