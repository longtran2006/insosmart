#How to dockerize the django with posgreSQL
https://wsvincent.com/django-docker-postgresql/'

* in the docker-compose.yml file originally it is:

  
    version: '3.6'

    services:
    db:
    image: postgres:10.6
    volumes:
      - postgres_data:/var/lib/postgresql/data/
    web:
        build: .
        command: python /insosmart/manage.py runserver 0.0.0.0:8000
        volumes:
          - .:/insosmart
        ports:
          - 8000:8000
        depends_on:
          - db
  
    volumes:
        postgres_data: 


but it need to remove the 'volumnes: -.:insosmart' 

create a docker volumne for this project:

    docker volume create --driver local --opt type=none --opt device=. --opt o=bind insosmart

